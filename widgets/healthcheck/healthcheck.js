widget = {
    //runs when we receive data from the job
    onData: function(el, data) {

        //The parameters our job passed through are in the data object
        //el is our widget element, so our actions should all be relative to that

        if (data.widgetTitle) {
            $('h2', el).text(data.widgetTitle.replace(/\.([^ ])/g, ". $1")); // put word breaks with periods in case of long hostnames
        }

        $('h3', el).text(data.url);

        var content = $('div.content', el);
        var a = $('a', content);

        a.attr('href', data.url);
        a.attr('target', '_blank');

        var indicator = $('div.indicator', a);
        indicator.removeClass('unknown healthy unhealthy');

        var details = $('p.details', content);

        if (data.isHealthy) {
            indicator.addClass('healthy');
            indicator.text('Healthy');
            if (data.tenants) details.text('Tenants: ' + data.tenants);
        } else {
            indicator.addClass('unhealthy');
            indicator.text('Unhealthy');
            details.text(data.failure);
        };
    }
};
