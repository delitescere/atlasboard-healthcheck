var expect = chai.expect;

var karmaHTML = document.body.innerHTML;
var template = __html__['healthcheck/healthcheck.html'];

describe('The health indicator', function() {
  var indicator;

  beforeEach(function(done) {
    document.body.innerHTML = '<div id="atlasboard">' + template + '</div>' + karmaHTML;
    done();
  });

  describe('when there is a failure it', function() {
    var failMessage = 'Typhoid Mary';

    beforeEach(function(done) {
      widget.onData($('#atlasboard'), { isHealthy: false, failure: failMessage });
      done();
    });

    it('sets indicator text to Unhealthy', function(done) {
      expect($('#indicator')).to.have.text('Unhealthy');
      done();
    });

    it('sets indicator class to unhealthy', function(done) {
      expect($('#indicator')).to.have.class('unhealthy');
      done();
    });

    it('sets details to failure message', function(done) {
      expect($('div.content p.details')).to.have.text(failMessage);
      done();
    });

  });

  describe('when healthy it', function() {
    var hosts = 1;

    beforeEach(function(done) {
      widget.onData($('#atlasboard'), { isHealthy: true, tenants: hosts });
      done();
    });

    it('sets indicator text to Healthy', function(done) {
      expect($('#indicator')).to.have.text('Healthy');
      done();
    });

    it('sets indicator class to healthy', function(done) {
      expect($('#indicator')).to.have.class('healthy');
      done();
    });

    it('sets details to number of tenants', function(done) {
      expect($('div.content p.details')).to.have.text('Tenants: ' + hosts);
      done();
    });

  });

  describe('when there is a widget title it', function() {

    it('sets h2 to the hostname but with breaking spaces', function(done) {
      widget.onData($('#atlasboard'), { widgetTitle: 'app.foo.com' });
      expect($('h2')).to.have.text('app. foo. com');
      done();
    });

    it('sets h2 to the application name', function(done) {
      widget.onData($('#atlasboard'), { widgetTitle: 'My App' });
      expect($('h2')).to.have.text('My App');
      done();
    });

  });

  describe('using the url it', function() {
    var url = 'https://app.foo.com/';

    beforeEach(function(done) {
      widget.onData($('#atlasboard'), { url: url });
      done();
    });

    it('sets h3 to the url', function(done) {
      expect($('h3')).to.have.text(url);
      done();
    });

    it('sets anchor to the url', function(done) {
      expect($('div.content a')).to.have.attr('href', url);
      done();
    });

    it('sets anchor to open a new tab', function(done) {
      expect($('div.content a')).to.have.attr('target', '_blank');
      done();
    });

  });

});

