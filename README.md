#Atlassian Health Check dashboard#

[![Build Status](https://drone.io/bitbucket.org/delitescere/atlasboard-healthcheck/status.png)](https://drone.io/bitbucket.org/delitescere/atlasboard-healthcheck/latest)

AtlasBoard package to monitor running Atlassian applications using their healthcheck resource.

![Atlassian Health Check](https://bitbucket.org/delitescere/atlasboard-healthcheck/raw/master/screenshots/healthcheck-example-dashboard.png)

The intention is for the team that develops a SaaS application can see the status of the basic running health of various instances of that application (development environment, staging, production, arbitrary instances) on their team wallboard. It is not intended for monitoring or recording statistics about the runtime characteristics of the application -- use a proper monitoring tool for that!

Assumes a JSON response from healtcheck resource that looks something like this:

    {
      "isHealthy": true,
      "name": "My application name"
    }

Multi-tenant applications that track the number of tenants they service may also include a "hosts" field like this:

    {
      "isHealthy": true,
      "name": "My application name",
      "hosts": 42
    }

##Installation##

From the root directory of your **recently created wallboard**, you just need to type:

    git init
    git submodule add https://bitbucket.org/delitescere/atlasboard-healthcheck packages/healthcheck

to import the package as **git submodule** and use any of the widgets and jobs in this package (check the healthcheck dashboard to see how).
