var URI = require('URIjs');

module.exports = function(config, dependencies, job_callback) {
  var logger = dependencies.logger;
  var hostname = URI(config.url).hostname();

  var options = {
    timeout: config.timeout || 5000,
    uri: config.url,
    headers: {
      Accept: "application/json"
    }
  }

  dependencies.request(options, function (error, response, body) {
    var data = {};

    if (error || !response || (response.statusCode != 200)) {
      var msg =
        (error ? '' + error : null) ||
        'Error: ' + (response ? 'Response status ' + response.statusCode : 'Unable to handle response');
      logger.error(msg);
      data = { widgetTitle: hostname, url: config.url, isHealthy: false, failure: msg};
      return job_callback(null, data);
    } else {
      var bodyJSON;

      try {
        bodyJSON = JSON.parse(body);
      } catch (err){
        var msg = 'Error: Unable to parse as JSON'
        logger.error(msg + ': ' + body);
        data = { widgetTitle: hostname, url: config.url, isHealthy: false, failure: msg};
        return job_callback(null, data);
      };

      data = {
        url: config.url,
        isHealthy: bodyJSON.isHealthy,
        widgetTitle: bodyJSON.name || hostname,
        tenants: bodyJSON.hosts
      };

      return job_callback(null, data);
    }
  });
};
